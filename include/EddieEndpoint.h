/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __COAPNODE_H__
#define __COAPNODE_H__

#include "CoapClient.h"
#include "CoapServer.h"
#include "EddieResource.h"

#include <string>

class EddieEndpoint {
private:
    CoapServer *server = nullptr;

    Site *site = nullptr;

    std::string resource_directory_ip;

public:

    EddieEndpoint();

    ~EddieEndpoint();

    CoapServer *get_server() {
        return server;
    }

    CoapClient *get_client() {
        return server->get_client();
    }

    /* get resource directory */
    int discover_rd();

    int get_resources_from_rd();

    int add_resource(EddieResource *resource);

    int start_server();

    /* publish resources into resource directory */
    int publish_resources();
};

#endif