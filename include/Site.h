/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __SITE_H__
#define __SITE_H__

#include "EddieResource.h"

#include <zephyr/net/socket.h>
#include <zephyr/net/coap.h>
#include <vector>
#include <functional>

typedef struct coap_resource coap_resource;

class Site {
private:

    std::vector<coap_resource> resources;

    static int sock;

    static int send_coap_reply(struct coap_packet *cpkt,
                const struct sockaddr *addr,
                socklen_t addr_len);

    static int well_known_core_get(struct coap_resource *resource,
                    struct coap_packet *request,
                    struct sockaddr *addr, socklen_t addr_len);

    static int resource_handler_get(struct coap_resource *resource,
                struct coap_packet *request,
                struct sockaddr *addr, socklen_t addr_len);

    static int resource_handler_put(struct coap_resource *resource,
                struct coap_packet *request,
                struct sockaddr *addr, socklen_t addr_len);

    static int resource_handler_post(struct coap_resource *resource,
                struct coap_packet *request,
                struct sockaddr *addr, socklen_t addr_len);

    static int send_response(message_t response_message, 
						struct coap_packet *request, 
						struct sockaddr *addr, 
						socklen_t addr_len);

public:

    Site();

    void set_sock(int sock);

    coap_resource* get_resources_arr();

    int add_resource(EddieResource *resource);

    const uint8_t* get_resources_in_linkformat(uint8_t *buffer, size_t buffer_size, uint16_t *length);
};

#endif