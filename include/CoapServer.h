/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __COAPSERVER_H__
#define __COAPSERVER_H__

#include "CoapClient.h"
#include "Site.h"

#define MAX_RETRANSMIT_COUNT 2

#define MAX_COAP_MSG_LEN 256

#define MY_COAP_PORT 5683

#define BLOCK_WISE_TRANSFER_SIZE_GET 2048

#define ALL_NODES_LOCAL_COAP_MCAST \
	{ { { 0xff, 0x02, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xfd } } }

#define NUM_PENDINGS 10


class CoapServer {
private:
    CoapClient *client;

    Site *site;

    /* CoAP socket fd */
    int sock;

    struct coap_pending pendings[NUM_PENDINGS];

    bool join_coap_multicast_group(void);
    
    int start_coap_server(void);

    int process_client_request(void);

    void process_coap_request(uint8_t *data, uint16_t data_len,
				 struct sockaddr *client_addr,
				 socklen_t client_addr_len);

public:

    CoapServer(Site *site);

    ~CoapServer();

    CoapClient *get_client();

    int publish_resources(const char * resource_directory_ip);

    int start_server();

};

#endif