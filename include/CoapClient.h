/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __COAPCLIENT_H__
#define __COAPCLIENT_H__

#include <zephyr/net/socket.h>
#include <functional>
#include <string>

#include "Common.h"

class CoapClient {
private:

    struct sockaddr_in6 destaddr;

    int start_coap_client(char const *dst_host);

    int process_simple_coap_reply(response_handler_t response_handler);

    int send_simple_coap_request(uint8_t method, 
                                const char * const path[], 
                                const char * const query[], 
                                const uint8_t data[], 
                                size_t data_length,
                                bool confirmable,
                                unsigned int content_format);
                                
    void close_coap_client();

    uint8_t method_to_coap_method(method_t method);

public:

    int send_request(request_t request, response_handler_t response_handler);
    
};

#endif