/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __EDDIERESOURCE_H__
#define __EDDIERESOURCE_H__

#include "Common.h"
#include <functional>

class EddieResource {
private:

public:
    virtual message_t handler_get(message_t request);
    virtual message_t handler_put(message_t request);
    virtual message_t handler_post(message_t request);
	virtual const char * const * get_path();
	virtual const char * const * get_attributes();
};

#endif