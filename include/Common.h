/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#include <string>
#include <functional>

typedef enum {GET, POST, PUT, DELETE} method_t;

typedef struct {
    char const * dst_host;
    method_t method;
    const char * const * path;
    const char * const * query;
    const uint8_t * data;
    size_t data_length;
    bool confirmable;
    unsigned int content_format;
} request_t;

typedef struct {
    const uint8_t *data; 
    uint16_t data_length;
    uint8_t status_code;
    std::string src_ip;
} message_t;

typedef std::function<void(message_t)> response_handler_t;

#endif