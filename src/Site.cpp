/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include "Site.h"
#include "EddieResource.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(site, LOG_LEVEL_DBG);

extern "C" {
	#include <net_private.h>
}

#include <zephyr/net/coap_link_format.h>
#include <functional>

#define MAX_COAP_MSG_LEN 256

////////////////////////////////// PRIVATE /////////////////////////////////

int Site::sock;

int Site::send_coap_reply(struct coap_packet *cpkt,
			   const struct sockaddr *addr,
			   socklen_t addr_len)
{
	int r;

	net_hexdump("Response", cpkt->data, cpkt->offset);

	r = sendto(sock, cpkt->data, cpkt->offset, 0, addr, addr_len);
	if (r < 0) {
		LOG_ERR("Failed to send %d", errno);
		r = -errno;
	}

	return r;
}

int Site::well_known_core_get(struct coap_resource *resource,
			       struct coap_packet *request,
			       struct sockaddr *addr, socklen_t addr_len)
{
	struct coap_packet response;
	uint8_t *data;
	int r;

	data = (uint8_t *)k_malloc(MAX_COAP_MSG_LEN);
	if (!data) {
		return -ENOMEM;
	}

	r = coap_well_known_core_get(resource, request, &response,
				     data, MAX_COAP_MSG_LEN);
	if (r < 0) {
		goto end;
	}

	r = send_coap_reply(&response, addr, addr_len);

end:
	k_free(data);

	return r;
}

int Site::send_response(message_t response_message, 
						struct coap_packet *request, 
						struct sockaddr *addr, 
						socklen_t addr_len) {
	struct coap_packet response;
	uint8_t token[COAP_TOKEN_MAX_LEN];
	uint8_t *data;
	uint16_t id;
	uint8_t tkl;
	int r;
	
	// make response packet
	id = coap_header_get_id(request);
	tkl = coap_header_get_token(request, token);

	data = (uint8_t *)k_malloc(MAX_COAP_MSG_LEN);
	if (!data) {
		return -ENOMEM;
	}

	r = coap_packet_init(&response, data, MAX_COAP_MSG_LEN,
				COAP_VERSION_1, COAP_TYPE_ACK, tkl, token,
				COAP_RESPONSE_CODE_CONTENT, id);
	if (r < 0) {
		r = -EINVAL;
		goto end;
	}

	r = coap_packet_append_payload_marker(&response);
	if (r < 0) {
		r = -EINVAL;
		goto end;
	}

	r = coap_packet_append_payload(&response, response_message.data, response_message.data_length);
	if (r < 0) {
		r = -EINVAL;
		goto end;
	}

	r = send_coap_reply(&response, addr, addr_len);

end:
	k_free(data);

	return r;
}

int Site::resource_handler_get(struct coap_resource *resource,
		    struct coap_packet *request,
		    struct sockaddr *addr, socklen_t addr_len)
{
	uint16_t payload_length;
	const uint8_t * payload = coap_packet_get_payload(request, &payload_length);
	uint8_t status_code = coap_header_get_code(request);
	message_t message_request {
		payload,
		payload_length,
		status_code,
		""
	};

	coap_core_metadata *metadata = static_cast<coap_core_metadata*>(resource->user_data);
	EddieResource *eddie_resource = static_cast<EddieResource*>(metadata->user_data);

	message_t response_message = eddie_resource->handler_get(message_request);
	return send_response(response_message, request, addr, addr_len);
}

int Site::resource_handler_put(struct coap_resource *resource,
		    struct coap_packet *request,
		    struct sockaddr *addr, socklen_t addr_len)
{
	uint16_t payload_length;
	const uint8_t * payload = coap_packet_get_payload(request, &payload_length);
	uint8_t status_code = coap_header_get_code(request);
	message_t message_request {
		payload,
		payload_length,
		status_code,
		""
	};

	coap_core_metadata *metadata = static_cast<coap_core_metadata*>(resource->user_data);
	EddieResource *eddie_resource = static_cast<EddieResource*>(metadata->user_data);

	message_t response_message = eddie_resource->handler_put(message_request);
	return send_response(response_message, request, addr, addr_len);
}

int Site::resource_handler_post(struct coap_resource *resource,
		    struct coap_packet *request,
		    struct sockaddr *addr, socklen_t addr_len)
{
	uint16_t payload_length;
	const uint8_t * payload = coap_packet_get_payload(request, &payload_length);
	uint8_t status_code = coap_header_get_code(request);
	message_t message_request {
		payload,
		payload_length,
		status_code,
		""
	};

	coap_core_metadata *metadata = static_cast<coap_core_metadata*>(resource->user_data);
	EddieResource *eddie_resource = static_cast<EddieResource*>(metadata->user_data);

	message_t response_message = eddie_resource->handler_post(message_request);
	return send_response(response_message, request, addr, addr_len);
}

////////////////////////////////// PUBLIC /////////////////////////////////

Site::Site() {
    coap_resource well_known_core_resource { 
        .get = well_known_core_get,
	    .path = COAP_WELL_KNOWN_CORE_PATH,
	};
    resources.push_back(well_known_core_resource);

    // the coap library wants the array of resources terminated by a NULL resource
    coap_resource empty_resource {};
    resources.push_back(empty_resource);
}

int Site::add_resource(EddieResource *resource) {
	
    coap_resource new_resource { 
        .get = resource_handler_get,
		.post = resource_handler_post,
		.put = resource_handler_put,
	    .path = resource->get_path(),
	};

	new_resource.user_data = new coap_core_metadata {
		.attributes = resource->get_attributes(),
		.user_data = resource // storing EddieResource ptr so that it can be accessed by the request handlers
	};

    resources.pop_back(); // pop NULL delimiter

    resources.push_back(new_resource);

    // the coap library wants the array of resources terminated by a NULL resource
    coap_resource empty_resource {};
    resources.push_back(empty_resource);

    return 0;
}

void Site::set_sock(int sock) {
	Site::sock = sock;
}

coap_resource* Site::get_resources_arr() {
    return &resources[0];
}


const uint8_t* Site::get_resources_in_linkformat(uint8_t *buffer, size_t buffer_size, uint16_t *length) {

	struct coap_packet request;
	struct coap_packet response;
	int r;
	
	r = coap_packet_init(&request, buffer, buffer_size, 
				 COAP_VERSION_1, COAP_TYPE_CON,
			     COAP_TOKEN_MAX_LEN, coap_next_token(),
			     COAP_METHOD_POST, coap_next_id());
	if (r < 0) {
		return NULL;
	}

	r = coap_well_known_core_get(get_resources_arr(), &request, &response,
				     buffer, buffer_size);
	if (r < 0) {
		return NULL;
	}

	const uint8_t * payload = coap_packet_get_payload(&response, length);
	if (payload) {
		net_hexdump("Publish resources Payload", payload, *length);
	}

	return payload;
}