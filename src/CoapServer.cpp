/*
 * Copyright (c) 2022 Huawei Inc.
 * Copyright (c) 2018 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <logging/log.h>
LOG_MODULE_REGISTER(coap_server, LOG_LEVEL_DBG);

#include "CoapServer.h"

#include <zephyr/net/coap.h>
#include <zephyr/net/coap_link_format.h>

extern "C" {
	#include <net_private.h>
	#include <ipv6.h>
}


////////////////// PRIVATE METHODS //////////////////////

bool CoapServer::join_coap_multicast_group(void)
{
	static struct in6_addr my_addr;
	static struct sockaddr_in6 mcast_addr = {
		.sin6_family = AF_INET6,
		.sin6_port = htons(MY_COAP_PORT),
		.sin6_addr = ALL_NODES_LOCAL_COAP_MCAST };
	struct net_if_addr *ifaddr;
	struct net_if *iface;
	int ret;

	iface = net_if_get_default();
	if (!iface) {
		LOG_ERR("Could not get te default interface\n");
		return false;
	}

	if (net_addr_pton(AF_INET6,
			  CONFIG_NET_CONFIG_MY_IPV6_ADDR,
			  &my_addr) < 0) {
		LOG_ERR("Invalid IPv6 address %s",
			CONFIG_NET_CONFIG_MY_IPV6_ADDR);
	}


	ifaddr = net_if_ipv6_addr_add(iface, &my_addr, NET_ADDR_MANUAL, 0);
	if (!ifaddr) {
		LOG_ERR("Could not add unicast address to interface");
		return false;
	}

	ifaddr->addr_state = NET_ADDR_PREFERRED;

	ret = net_ipv6_mld_join(iface, &mcast_addr.sin6_addr);
	if (ret < 0) {
		LOG_ERR("Cannot join %s IPv6 multicast group (%d)",
			net_sprint_ipv6_addr(&mcast_addr.sin6_addr), ret);
		return false;
	}

	LOG_DBG("joined multicast group");
	return true;
}

int CoapServer::start_coap_server(void)
{
	struct sockaddr_in6 addr6;
	int r;

	join_coap_multicast_group();

	memset(&addr6, 0, sizeof(addr6));
	addr6.sin6_family = AF_INET6;
	addr6.sin6_port = htons(MY_COAP_PORT);

	sock = socket(addr6.sin6_family, SOCK_DGRAM, IPPROTO_UDP);
	if (sock < 0) {
		LOG_ERR("Failed to create UDP socket %d", errno);
		return -errno;
	}

	r = bind(sock, (struct sockaddr *)&addr6, sizeof(addr6));
	if (r < 0) {
		LOG_ERR("Failed to bind UDP socket %d", errno);
		return -errno;
	}

	return 0;
}

void CoapServer::process_coap_request(uint8_t *data, uint16_t data_len,
				 struct sockaddr *client_addr,
				 socklen_t client_addr_len) {
	struct coap_packet request;
	struct coap_pending *pending;
	struct coap_option options[16] = { 0 };
	uint8_t opt_num = 16U;
	uint8_t type;
	int r;

	r = coap_packet_parse(&request, data, data_len, options, opt_num);
	if (r < 0) {
		LOG_ERR("Invalid data received (%d)\n", r);
		return;
	}

	type = coap_header_get_type(&request);

	pending = coap_pending_received(&request, pendings, NUM_PENDINGS);
	if (pending) {
		/* Clear CoAP pending request */
		if (type == COAP_TYPE_ACK || type == COAP_TYPE_RESET) {
			k_free(pending->data);
			coap_pending_clear(pending);
		}
	}
	else {
		r = coap_handle_request(&request, this->site->get_resources_arr(), options, 
					opt_num, client_addr, client_addr_len);
		if (r < 0) {
			LOG_WRN("No handler for such request (%d)\n", r);
		}
	}
}

int CoapServer::process_client_request(void)
{
	int received;
	struct sockaddr client_addr;
	socklen_t client_addr_len;
	uint8_t request[MAX_COAP_MSG_LEN];

	do {
		client_addr_len = sizeof(client_addr);
		received = recvfrom(sock, request, sizeof(request), 0,
				    &client_addr, &client_addr_len);
		if (received < 0) {
			LOG_ERR("Connection error %d", errno);
			return -errno;
		}

		process_coap_request(request, received, &client_addr,
				     client_addr_len);
	} while (true);

	return 0;
}

///////////// PUBLIC METHODS ////////////

CoapServer::CoapServer(Site *site) {
    this->client = new CoapClient();
	int r = start_coap_server();
	if (r<0) LOG_ERR("Could not start coap server");

	this->site = site;
}

CoapServer::~CoapServer() {
	delete this->client;
	delete this->site;
}

CoapClient* CoapServer::get_client() {
	return client;
}

int CoapServer::start_server() {
	int res;

	site->set_sock(this->sock);

	while (1) {
		res = process_client_request();
		if (res < 0) {
            LOG_ERR("Error processing client request");
			return -1;
		}
	}

    return 0;
}
