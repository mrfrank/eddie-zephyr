/*
 * Copyright (c) 2022 Huawei Inc.
 * Copyright (c) 2018 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <logging/log.h>
LOG_MODULE_REGISTER(coap_client, LOG_LEVEL_DBG);

#include "CoapClient.h"

#include <net_private.h>
#include <zephyr/net/coap.h>


#define PEER_PORT 5683
#define MAX_COAP_MSG_LEN 512

/* CoAP socket fd */
static int sock;

////////////////// PRIVATE METHODS ///////////////////////

int CoapClient::start_coap_client(char const *dst_host)
{
	destaddr.sin6_family = AF_INET6;
	destaddr.sin6_port = htons(PEER_PORT);
	destaddr.sin6_scope_id = 0U;

	inet_pton(AF_INET6, dst_host, &destaddr.sin6_addr);

	sock = socket(destaddr.sin6_family, SOCK_DGRAM, IPPROTO_UDP);
	if (sock < 0) {
		LOG_ERR("Failed to create UDP socket %d", errno);
		return -errno;
	}

	// Listen on all interfaces on port 8201 
	struct sockaddr_in6 interface_addr;
	memset(&interface_addr, 0, sizeof(interface_addr));
	interface_addr.sin6_family = AF_INET6;
	interface_addr.sin6_port = htons(8201);
	interface_addr.sin6_addr = IN6ADDR_ANY_INIT;

	if (bind(sock, (struct sockaddr *)&interface_addr,
		sizeof(interface_addr)) < 0) {
		LOG_ERR("bind error: %d", errno);
		return -errno;
	}

	return 0;
}

int CoapClient::process_simple_coap_reply(response_handler_t response_handler)
{
	struct coap_packet reply;
	int ret;
	sockaddr src_addr;
	socklen_t addrlen;

	uint8_t * data = (uint8_t *)k_malloc(MAX_COAP_MSG_LEN);
	if (!data) {
		return -ENOMEM;
	}

	int rcvd = recvfrom(sock, data, MAX_COAP_MSG_LEN, MSG_WAITALL, &src_addr, &addrlen);

	if (rcvd <= 0) {
		LOG_ERR("recv error: %d", rcvd);
		k_free(data);
		return -errno;
	}

	// retrieve source ip
	char source_ip[64];
	inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)&src_addr)->sin6_addr), source_ip, 64);
	LOG_DBG("source ip: %s", source_ip);

	ret = coap_packet_parse(&reply, data, rcvd, NULL, 0);
	if (ret < 0) {
		LOG_ERR("Invalid data received");
		k_free(data);
		return ret;
	}

	uint16_t payload_length;
	const uint8_t * payload = coap_packet_get_payload(&reply, &payload_length);
	uint8_t status_code = coap_header_get_code(&reply);

	message_t response_message {
		payload,
		payload_length,
		status_code,
		std::string(source_ip)
	};
	response_handler(response_message);

	k_free(data);
	return 0;
}

int CoapClient::send_simple_coap_request(uint8_t method, 
								const char * const path[], 
								const char * const query[], 
								const uint8_t payload[], 
								size_t payload_length, 
								bool confirmable,
								unsigned int content_format)
{
	struct coap_packet request;
	const char * const *p;
	uint8_t *data;
	int r = 0;

	data = (uint8_t *)k_malloc(MAX_COAP_MSG_LEN);
	if (!data) {
		return -ENOMEM;
	}

	r = coap_packet_init(&request, data, MAX_COAP_MSG_LEN,
			     COAP_VERSION_1, confirmable ? COAP_TYPE_CON : COAP_TYPE_NON_CON,
			     COAP_TOKEN_MAX_LEN, coap_next_token(),
			     method, coap_next_id());
	if (r < 0) {
		LOG_ERR("Failed to init CoAP message");
		goto end;
	}

	for (p = path; p && *p; p++) {
		r = coap_packet_append_option(&request, COAP_OPTION_URI_PATH,
					      (const uint8_t*) *p, strlen(*p));
		if (r < 0) {
			LOG_ERR("Unable add path option to request");
			goto end;
		}
	}

	r = coap_append_option_int(&request, COAP_OPTION_CONTENT_FORMAT, content_format);
	if (r < 0) {
		return -EINVAL;
	}

	for (p = query; p && *p; p++) {
		r = coap_packet_append_option(&request, COAP_OPTION_URI_QUERY,
					      (const uint8_t*) *p, strlen(*p));
		if (r < 0) {
			LOG_ERR("Unable add query option to request");
			goto end;
		}
	}

	switch (method) {
	case COAP_METHOD_GET:
	case COAP_METHOD_DELETE:
		break;

	case COAP_METHOD_PUT:
	case COAP_METHOD_POST:
		r = coap_packet_append_payload_marker(&request);
		if (r < 0) {
			LOG_ERR("Unable to append payload marker");
			goto end;
		}

		r = coap_packet_append_payload(&request, (uint8_t *)payload, payload_length);
		if (r < 0) {
			LOG_ERR("Not able to append payload");
			goto end;
		}

		break;
	default:
		r = -EINVAL;
		goto end;
	}

	net_hexdump("Request", request.data, request.offset);

	if(sendto(sock, request.data, request.offset, 0, (struct sockaddr*)&destaddr,
       sizeof(destaddr)) < 0) {
		LOG_ERR("sendto error");
		goto end;
	};

end:
	k_free(data);

	return r;
}

void CoapClient::close_coap_client() {
	(void)close(sock);
}

uint8_t CoapClient::method_to_coap_method(method_t method) {
	switch (method) {
	case GET:
		return COAP_METHOD_GET;
	case POST:
		return COAP_METHOD_POST;
	case PUT:
		return COAP_METHOD_PUT;
	case DELETE:
		return COAP_METHOD_DELETE;
	}
	return -1;
}

/////////////// PUBLIC METHODS /////////////

int CoapClient::send_request(
		request_t request, 
		response_handler_t response_handler
) {

	start_coap_client(request.dst_host);

	LOG_DBG("destination address: %s \n", request.dst_host);

	uint8_t coap_method = method_to_coap_method(request.method);

	send_simple_coap_request(coap_method, 
                                request.path, 
                                request.query, 
                                request.data, 
                                request.data_length,
                                request.confirmable,
								request.content_format);

    int r = process_simple_coap_reply(response_handler);
    if (r < 0) {
        LOG_ERR("Error processing coap reply");
        return -1;
    }

	close_coap_client();

	return 0;
}