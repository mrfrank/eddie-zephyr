/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <logging/log.h>
LOG_MODULE_REGISTER(eddie_endpoint, LOG_LEVEL_DBG);

#include "EddieEndpoint.h"
#include "CoapClient.h"
#include "CoapServer.h"
#include "Common.h"
#include "EddieResource.h"

#include <net_private.h>

int EddieEndpoint::discover_rd()
{
	int r;

	const char * const path[] = { ".well-known", "core", NULL };
	const char * const query[] = { "rt=core.rd", NULL };
	const uint8_t payload[] = {};
	char const * dst_host = "FF05::FD";

	request_t get_resources_request = {
		dst_host,
		GET,
		path,
		query,
		payload,
		0,			//data_length
		false,		//confirmable
		40			//linkformat content format
	};

	auto response_handler = [this](message_t response_message) {
		this->resource_directory_ip = response_message.src_ip;
		LOG_DBG("Resource directory ip: %s \n", this->resource_directory_ip.c_str());
	};

	r = get_client()->send_request(get_resources_request, response_handler);

	return 0;
}

int EddieEndpoint::add_resource(EddieResource *resource) {
	this->site->add_resource(resource);
	return 0;
}

int EddieEndpoint::start_server() 
{
	return server->start_server();
}

EddieEndpoint::EddieEndpoint() {
	this->site = new Site();
    this->server = new CoapServer(this->site);
}

EddieEndpoint::~EddieEndpoint() {
    delete server;
}

int EddieEndpoint::publish_resources() {
	uint8_t *data;
	int r;

	// generate payload
	data = (uint8_t *)k_malloc(MAX_COAP_MSG_LEN);
	if (!data) {
		return -ENOMEM;
	}
	uint16_t payload_length;
	const uint8_t* payload = this->site->get_resources_in_linkformat(data, MAX_COAP_MSG_LEN, &payload_length);

	if (payload == NULL) {
		k_free(data);
		return -1;
	}

	// define request path and query
	const char * const path[] = { "rd", NULL };
	const char * const query[] = { 
									"base=coap://[" CONFIG_NET_CONFIG_MY_IPV6_ADDR "]:5683" , 
									"ep=zephyrnode", // TODO: use device id as endpoint name
									"lt=90000",
									NULL 
								};

	request_t publish_resources_request = {
		this->resource_directory_ip.c_str(), 	//dst_host
		POST, 									//method
		path,									//path
		query,									//query
		payload,								//data
		payload_length,							//data_length
		true,									//confirmable
		40										//linkformat
	};

	auto response_handler = [](message_t response_message) {
		net_hexdump("Response", response_message.data, response_message.data_length);
	};

	r = get_client()->send_request(publish_resources_request, response_handler);
	if (r < 0) {
		k_free(data);
		return r;
	}

	k_free(data);
	return 0;
}

int EddieEndpoint::get_resources_from_rd() {
	int r;

	const char * const path[] = { "rd-lookup", "res", NULL };
	const char * const query[] = { NULL };
	const uint8_t payload[] = {};
	char const * dst_host = this->resource_directory_ip.c_str();

	request_t get_resources_request = {
		dst_host,
		GET,
		path,
		query,
		payload,
		0,			//data_length
		true,		//confirmable
		40			//linkformat content format
	};

	auto response_handler = [](message_t response_message) {
		net_hexdump("Response", response_message.data, response_message.data_length);
		// TODO parse links and return them
	};

	r = get_client()->send_request(get_resources_request, response_handler);

	return 0;
}
