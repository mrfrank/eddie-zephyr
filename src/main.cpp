/*
 * Copyright (c) 2022 Huawei Inc.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include "EddieEndpoint.h"
#include "EddieResource.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

#ifdef CONFIG_NET_L2_OPENTHREAD
	#include <zephyr/net/openthread.h>

	int connected = false;

	void ot_state_changed_callback(otChangedFlags aFlags, void *aContext) {
		if(aFlags == OT_CHANGED_THREAD_NETDATA)
			connected = true;
	}

	void wait_for_networking() {
		openthread_set_state_changed_cb(ot_state_changed_callback);

		while(!connected) k_sleep(K_SECONDS(1));
	}
#else
	void wait_for_networking() {
		k_sleep(K_SECONDS(1));
	}
#endif

class EddieLamp : public EddieResource {
private:
	const char * const core_path[2] = { "zephyr-lamp", NULL };
	const char * const core_attributes[3] = { "rt=eddie.lamp", "ct=40", NULL };

public:

	message_t handler_get(message_t request) {
		message_t response;
		const char * response_string = "test get";
		response.data = (const uint8_t *)response_string;
		response.data_length = strlen(response_string);
		return response;
	}

	message_t handler_put(message_t request) {
		message_t response;
		const char * response_string = "test put";
		response.data = (const uint8_t *)response_string;
		response.data_length = strlen(response_string);
		return response;
	}

	message_t handler_post(message_t request) {
		message_t response;
		const char * response_string = "test post";
		response.data = (const uint8_t *)response_string;
		response.data_length = strlen(response_string);
		return response;
	}

	const char * const* get_path() {
		return this->core_path;
	}

	const char * const* get_attributes() {
		return this->core_attributes;
	}
};

static void start_eddie() {
	EddieEndpoint node = EddieEndpoint();
	EddieLamp lamp_resource;

	node.discover_rd();
	node.add_resource(&lamp_resource);
	node.publish_resources();
	node.get_resources_from_rd();
	node.start_server();
}

int main(void)
{
	wait_for_networking();
	start_eddie();
	
	return 0;
}
