<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

# Eddie Blueprint Zephyr Application

This is the Eddie Zephyr Node which is part of the [Eddie Blueprint](https://booting.oniroproject.org/distro/blueprints/eddie/meta-oniro-blueprints-eddie).

## Our goal
The objective of the EDDIE project is to design
a middleware running on top of the operating system of the various Oniro
devices. Capable of giving to the applications and services an global view
on a unified pool of virtual resources that can be exploited for an efficient
execution of any application or service required by the user.

For more information, see [this epic](https://gitlab.eclipse.org/groups/eclipse/oniro-core/-/epics/43).

## Description
A device node provides its resources by means of coap endpoints, 
The Zephyr node will publish its resources into the [resource directory on linux](https://booting.oniroproject.org/antonio.miele/eddie) using Coap protocol. After publishing the resource, the zephyr node will run a Coap server providing an endpoint specified by the published resource. 

We tested both the resource publish and the Coap server endpoint, in the following setups:
- Two Qemu instances with bridged networking, one Qemu running Eddie Zephyr and the other running the eddie-server service on Oniro Linux
- An Eddie Zephyr node on the Arduino Nano 33 BLE board connected via OpenThread to a Raspberry PI OpenThread border router that runs eddie-server service 

## Run on Qemu
If you want to have Qemu with networking, create one tap device for each Qemu instance (you can use a script provided in oe-core):
```console
$ sudo oe-core/scripts/runqemu-gen-tapdevs 1000 1000 2 tmp/sysroots-components/x86_64/qemu-helper-native/usr/bin
```
Bridge tap devices so the Qemu instances can communicate with each other
```console
$ sudo ip link add br0 type bridge
$ sudo ip link set br0 up
$ sudo ip addr add 2001:db8:0:1::100/64 dev br0
$ sudo ip link set tap0 master br0
$ sudo ip link set tap1 master br0
```
Assuming the linux node with Eddie service is running on tap0 network interface, use the following command to run the zephyr node on Qemu. 

```console
$ west build -b qemu_x86 -t run --pristine  --    -DOVERLAY_CONFIG=overlay-e1000.conf  -DCONFIG_NET_CONFIG_MY_IPV6_ADDR=\"2001:db8:0:1::1\"    -DCONFIG_NET_CONFIG_PEER_IPV6_ADDR=\"2001:db8:0:1::2\"     -DCONFIG_ETH_QEMU_IFACE_NAME=\"tap1\"    -DCONFIG_ETH_QEMU_EXTRA_ARGS=\"mac=00:00:5e:00:53:01\"
```

The output logs look as follows
```
[00:00:00.010,000] <inf> net_config: Initializing network
[00:00:00.110,000] <inf> net_config: IPv6 address: 2001:db8:0:1::1
[00:00:00.110,000] <inf> net_config: IPv6 address: 2001:db8:0:1::1
[00:00:03.120,000] <dbg> net_coap_client_sample: net_hexdump: Request
                                                 48 02 d1 dd 80 1f 09 01  56 f6 a3 3c b2 72 64 4d |H.��.... V.�<.rdM
                                                 00 68 3d 74 65 6d 70 65  72 61 74 75 72 65 0d 07 |.h=tempe rature..
                                                 61 64 64 72 3d 32 30 30  31 3a 64 62 38 3a 30 3a |addr=200 1:db8:0:
                                                 31 3a 3a 31 09 70 6f 72  74 3d 35 36 38 33 ff    |1::1.por t=5683. 
[00:00:03.150,000] <dbg> net_coap_client_sample: net_hexdump: Response
                                                 70 00 d1 dd                                      |p.��             
```

## Run on Arduino Nano 33 BLE
- Build with the OpenThread overlay
```console
$ west build -b arduino_nano_33_ble --  -DOVERLAY_CONFIG=overlay-openthread.conf
```
- Flash the image into your arduino board [using bossac](https://docs.zephyrproject.org/2.7.0/boards/arm/arduino_nano_33_ble/doc/index.html)
- Run the Eddie Linux node on the Raspberry Pi board following the instructions [here](https://booting.oniroproject.org/distro/blueprints/eddie/meta-oniro-blueprints-eddie).

- Follow the [transparent gateway documentation](https://docs.oniroproject.org/projects/blueprints/en/latest/transparent-gateway.html) to connect the Arduino board to the Raspberry Pi via OpenThread.

The Zephyr node will be able to publish its resource to the Linux node.